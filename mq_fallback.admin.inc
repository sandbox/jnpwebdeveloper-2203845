<?php

/**
 * @file
 * Contains the administration pages for Media Query Fallback module.
 */

/**
 * Implements hook_form_FORM_ID().
 */
function mq_fallback_settings_form() {
  $form['mq_fallback_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Media Query Fallback'),
    '#default_value' => variable_get('mq_fallback_enabled', FALSE),
  );
  $form['mq_fallback_breakpoint_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Breakpoint Width (px)'),
    '#default_value' => variable_get('mq_fallback_breakpoint_width', 1000),
    '#description' => t('Default is 1000px. Examples: 1000px for desktop,760px for tablet or 320px for mobile. Width varies and will depend on your setup. All matching media query blocks will be provided in a conditional stylesheet for < IE9.'),
    '#size' => 6,
    '#maxlength' => 5,
    '#required' => TRUE,
  );
  $form['mq_fallback_devel'] = array(
    '#type' => 'checkbox',
    '#title' => t('Devel Mode'),
    '#description' => t('CSS files are checked for modification on every page request and Fallback CSS files are rebuilt Turn this off on production.'),
    '#default_value' => variable_get('mq_fallback_devel', FALSE),
  );

  $form['#submit'][] = 'mq_fallback_form_submit_rebuild';

  return system_settings_form($form);
}

/**
 * Custom form validation handler.
 */
function mq_fallback_settings_form_validate($form, &$form_state) {
  if ($form_state['values']['mq_fallback_breakpoint_width'] == '' || !is_numeric($form_state['values']['mq_fallback_breakpoint_width'])) {
    form_set_error('mq_fallback_breakpoint_width', t('Please enter a valid width in pixels.'));
  }
}

/**
 * Custom form submit handler.
 */
function mq_fallback_form_submit_rebuild($form, &$form_state) {
  $enabled = $form_state['value']['mq_fallback_enabled'];
  variable_set('mq_fallback_enabled', $enabled);

  // Either way if enabled/disabled, let's flush the cache.
  drupal_flush_all_caches();
}
