
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Troubleshooting
 * Credits


INTRODUCTION
------------
The Media Query Fallback module provides server side media query support for 
legacy browsers. 

Currently the module provides support for IE8. IE7 is targeted but this has 
not been tested.

You can specify a set width that the legacy browser should be displayed in.

The CSS files are parsed and the media query blocks that match that breakpoint 
are extracted and provided in a conditional < stylesheet.

Compatible with, Aggegation, Less and the CDN module.

INSTALLATION
------------
Simply enable the module, then enable Media Query Fallback in the 
settings page /admin/config/development/mq_fallback

Ensure to clear your cache to make sure all media queries have been rebuilt.

TROUBLESHOOTING
---------------
- Try turning on "Devel Mode" on the settings page
- Inspect the generated CSS to see if some CSS is missing

SPONSOR
-------
Proudly sponsored by Comic Relief (www.comicrelief.com)

CREDITS
-------
* Developer: Jeremy Pitt (@jnpwebdeveloper), www.jnpwebdeveloper.com

* Cainmi for use of his media query regex function 
https://github.com/cainmi/
